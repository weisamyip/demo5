import { Moment } from 'moment';

export interface IBook {
  id?: number;
  title?: string;
  description?: string;
  publicationDate?: Moment;
}

export class Book implements IBook {
  constructor(public id?: number, public title?: string, public description?: string, public publicationDate?: Moment) {}
}

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { Demo5SharedModule } from 'app/shared/shared.module';
import { Demo5CoreModule } from 'app/core/core.module';
import { Demo5AppRoutingModule } from './app-routing.module';
import { Demo5HomeModule } from './home/home.module';
import { Demo5EntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ErrorComponent } from './layouts/error/error.component';

@NgModule({
  imports: [
    BrowserModule,
    Demo5SharedModule,
    Demo5CoreModule,
    Demo5HomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    Demo5EntityModule,
    Demo5AppRoutingModule,
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, FooterComponent],
  bootstrap: [MainComponent],
})
export class Demo5AppModule {}
